module Test
  class ClassThatWillLeakSecretSoon
    def initialize(token = 'AWS_KEY')
     @token = token
     @other_field = 123123
     @password = "zxc31w1xzAA"
    end
  end
end
